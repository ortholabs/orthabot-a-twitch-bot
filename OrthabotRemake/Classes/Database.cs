﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OrthabotRemake
{
    public static class Database
    {
        public static SQLiteConnection dbcon;
        static string connectionString = "Data Source=OrthaBot.sqlite;Version=3;";

        static int dbVersion = 0;

        public static void Init()
        {
            var newDB = false;
            if (!File.Exists("OrthaBot.sqlite"))
            {
                newDB = true;
                SQLiteConnection.CreateFile("OrthaBot.sqlite");
            }

            if (newDB)
            {
                UpdateDatabase(-1);
            }
            else
            {
                //check for update
                using (var reader = Command("SELECT * FROM config"))
                {
                    var foundVer = false;
                    while (reader.Read())
                    {
                        var ver = (int)(Int64)reader.data["ver"];

                        if(ver != dbVersion)
                            UpdateDatabase(ver);
                        foundVer = true;
                    }

                    if(!foundVer)
                        UpdateDatabase(-1);
                }
            }
        }

        public static void Open()
        {
            dbcon = new SQLiteConnection(connectionString);
            dbcon.Open();
        }

        static void UpdateDatabase(int oldVersion)
        {
            if(oldVersion == -1)
                BotForm.Write("  -Creating Database from scratch.");
            else
                BotForm.Write("  -Updating DB from v" + oldVersion + " to v" + dbVersion + ".");

            //doing EVERYTHING from scratch
            if(oldVersion < 0)
            {
                //doing all input before touching the DB for easy rollback
                var channel = new InputBox("Channel to connect to:").value;
                var botName = new InputBox("Bot's name (registered by you):").value;
                var botOauth = new InputBox("Bot Oauth:", true).value;

                //spooky
                Open();
                new SQLiteCommand("DROP TABLE IF EXISTS config", dbcon).ExecuteNonQuery();
                new SQLiteCommand("DROP TABLE IF EXISTS modules", dbcon).ExecuteNonQuery();
                new SQLiteCommand("DROP TABLE IF EXISTS customCommands", dbcon).ExecuteNonQuery();
                new SQLiteCommand("DROP TABLE IF EXISTS storedMods", dbcon).ExecuteNonQuery();

                new SQLiteCommand("CREATE TABLE config (pkey INTEGER PRIMARY KEY, ver INTEGER, channel VARCHAR(100), botName VARCHAR(64), botOauth VARCHAR(255))", dbcon).ExecuteNonQuery();
                new SQLiteCommand("CREATE TABLE modules (pkey INTEGER PRIMARY KEY, moduleName VARCHAR(100), moduleVersion INTEGER, moduleEnabled INTEGER)", dbcon).ExecuteNonQuery();
                new SQLiteCommand("CREATE TABLE customCommands (name VARCHAR(255) PRIMARY KEY, say VARCHAR(255))", dbcon).ExecuteNonQuery();
                new SQLiteCommand("CREATE TABLE storedMods (pkey INTEGER PRIMARY KEY, name VARCHAR(255))", dbcon).ExecuteNonQuery(); //clear out all mods, and then add new ones, or selectively remove ones

                //set the version to be 0, in case the next potential version doesn't finish, we can resume with them
                var command = new SQLiteCommand("REPLACE INTO config (pkey, ver, channel, botName, botOauth) VALUES (0, @ver, @chan, @name, @oauth)", dbcon);
                command.Parameters.AddWithValue("@ver", 0);
                command.Parameters.AddWithValue("@chan", channel);
                command.Parameters.AddWithValue("@name", botName);
                command.Parameters.AddWithValue("@oauth", botOauth);
                command.ExecuteNonQuery();

                foreach (var moduleBase in IRC.modules)
                {
                    if(string.IsNullOrWhiteSpace(moduleBase.moduleName.Trim())) continue;
                    new SQLiteCommand("INSERT INTO modules (moduleName, moduleVersion, moduleEnabled) VALUES ('" + moduleBase.moduleName +"'," + moduleBase.moduleVersion + "," + (moduleBase.enabled ? 1 : 0) + ")", dbcon).ExecuteNonQuery();
                }

                Close();
            }
            //etc, same for each new version where the DB changes.
        }

        public static void SetupConfig()
        {
            var channel = new InputBox("Channel to connect to:").value;
            var botName = new InputBox("Bot's name (registered by you):").value;
            var botOauth = new InputBox("Bot Oauth:", true).value;

            Open();
            var command = new SQLiteCommand("REPLACE INTO config (pkey, channel, botName, botOauth) VALUES (0, @chan, @name, @oauth)", dbcon);
            command.Parameters.AddWithValue("@chan", channel);
            command.Parameters.AddWithValue("@name", botName);
            command.Parameters.AddWithValue("@oauth", botOauth);
            command.ExecuteNonQuery();
            Close();
        }

        private static void DeleteDB()
        {
            Close();
            if (File.Exists("OrthaBot.sqlite")) { File.Delete("OrthaBot.sqlite"); }
        }

        /*Proper use:
        var botName = "unset";
        using (var reader = Command("SELECT * FROM config"))
        {
            while (reader.Read())
            {
                botName = (string)reader.data["botName"];
            }
        }*/
        public static DbReader Command(string cmd)
        {
            Open();

            var dbcmd = dbcon.CreateCommand(); dbcmd.CommandText = cmd;
            var reader = dbcmd.ExecuteReader();
            dbcmd.Dispose();

            return new DbReader(){data = reader};
        }

        public static int CommandNonQuery(string cmd)
        {
            Open();

            var dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = cmd;
            var ret = dbcmd.ExecuteNonQuery();

            dbcmd.Dispose();
            Close();

            return ret;
        }

        public static void Close()
        {
            dbcon.Close();
        }

        public class DbReader : IDisposable
        {
            public IDataReader data;

            public bool Read() { return data.Read(); }

            public void Dispose()
            {
                data.Close();
                Close();
            }
        }
    }
}
