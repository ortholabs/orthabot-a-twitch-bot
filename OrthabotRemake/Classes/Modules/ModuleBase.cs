﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace OrthabotRemake
{
    public class ModuleBase
    {
        public bool enabled = true;

        public string handlesCommands = "";
        public string moduleName = "";
        public int moduleVersion = 0;

        //returns if this module uses the command given
        public virtual bool HandleCommand(string name, bool isMod, bool isGlobalOrOwner, string command, string parameters) { return false; }
        public virtual void AddedLog(string line, Color col){}
        public virtual void FlushedLog(string log){}

        public void Say(string message)
        {
            BotForm.instance.twitch.Say(message);
        }
    }
}
