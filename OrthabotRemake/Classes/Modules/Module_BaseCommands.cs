﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrthabotRemake
{
    class Module_BaseCommands : ModuleBase
    {
        //Never going to be able to disable this one
        public new bool enabled { get { return true; } }

        public Module_BaseCommands()
        {
            handlesCommands = "AddMod, RemoveMod, ListMods, Say, List, EnableModule, DisableModule, ListModules";
        }

        public override bool HandleCommand(string name, bool isMod, bool isGlobalOrOwner, string command, string parameters)
        {
            var splitParams = parameters.Split(' ');

            switch (command)
            {
                case "addmod":
                    {
                        if (!isGlobalOrOwner)
                            return false;

                        Database.Open();

                        var dbcmd = Database.dbcon.CreateCommand(); dbcmd.CommandText = "REPLACE INTO storedMods (name) VALUES (@name)";
                        dbcmd.Parameters.AddWithValue("@name", splitParams[0].ToLower());
                        var success = dbcmd.ExecuteNonQuery();

                        Database.Close();

                        if (success > 0)
                        {
                            Say("Added " + splitParams[0] + " to list of mod users.");
                            return true;
                        }
                    }
                    break;
                case "removemod":
                    {
                        if (!isGlobalOrOwner)
                            return false;

                        Database.Open();

                        var dbcmd = Database.dbcon.CreateCommand(); dbcmd.CommandText = "DELETE FROM storedMods WHERE name = @name";
                        dbcmd.Parameters.AddWithValue("@name", splitParams[0].ToLower());
                        var success = dbcmd.ExecuteNonQuery();

                        Database.Close();

                        if (success > 0)
                        {
                            Say("Deleted " + splitParams[0] + " from list of custom commands.");
                            return true;
                        }
                    }
                    break;
                case "listmods":
                    {
                        if (!isGlobalOrOwner)
                            return false;

                        var str = "Stored mods: ";
                        using (var reader = Database.Command("SELECT * FROM storedMods"))
                        {
                            while (reader.Read())
                            {
                                str += (string)reader.data["name"] + ", ";
                            }
                        }
                        str = str.Substring(0, str.Length - 2);

                        if (str.Length < "Stored mods: ".Length)
                            str = "No stored mods, you can add them with !addmod name. This overrides the mod flag in IRC. Can be applied to non-mods to give them mod access for the bot.";

                        Say(str);

                        return true;
                    }
                case "say":
                    {
                        if (!isGlobalOrOwner)
                            return false;

                        Say(parameters);
                        return true;
                    }
                case "list":
                    {
                        if (!isGlobalOrOwner && !isMod)
                            return false;

                        var str = "All possible non-custom commands: ";

                        foreach (var moduleBase in IRC.modules)
                        {
                            if(moduleBase.enabled && !string.IsNullOrWhiteSpace(moduleBase.handlesCommands.Trim()))
                            {
                                str += moduleBase.handlesCommands + ", ";
                            }
                        }
                        str = str.Substring(0, str.Length - 2);

                        if (str.Length < "All possible non-custom commands: ".Length)
                            str = "No possible commands? You couldn't even do this. Something's broke yo.";

                        Say(str);

                        return true;
                    }
                case "enablemodule":
                    {
                        var paramName = splitParams[0];
                        ModuleBase module = default(ModuleBase);
                        foreach (var moduleBase in IRC.modules)
                        {
                            if(moduleBase.moduleName.ToLower().Equals(paramName.ToLower()))
                                module = moduleBase;
                        }
                        if(module == default(ModuleBase))
                        {
                            Say("No module '" + paramName + "' found. Use !listmodules to see the available modules.");
                            return true;
                        }

                        Database.Open();

                        var dbcmd = Database.dbcon.CreateCommand(); dbcmd.CommandText = "UPDATE modules SET moduleEnabled = @enabled WHERE moduleName = @name";
                        dbcmd.Parameters.AddWithValue("@name", module.moduleName);
                        dbcmd.Parameters.AddWithValue("@enabled", 1);
                        var success = dbcmd.ExecuteNonQuery();

                        Database.Close();

                        if (success > 0)
                        {
                            module.enabled = true;
                            Say("Enabled the " + paramName + " module.");
                            return true;
                        }
                    }
                    break;
                case "disablemodule":
                    {
                        var paramName = splitParams[0];
                        ModuleBase module = default(ModuleBase);
                        foreach (var moduleBase in IRC.modules)
                        {
                            if (moduleBase.moduleName.ToLower().Equals(paramName.ToLower()))
                                module = moduleBase;
                        }
                        if (module == default(ModuleBase))
                        {
                            Say("No module '" + paramName + "' found. Use !listmodules to see the available modules.");
                            return true;
                        }

                        Database.Open();

                        var dbcmd = Database.dbcon.CreateCommand(); dbcmd.CommandText = "UPDATE modules SET moduleEnabled = @enabled WHERE moduleName = @name";
                        dbcmd.Parameters.AddWithValue("@name", module.moduleName);
                        dbcmd.Parameters.AddWithValue("@enabled", 0);
                        var success = dbcmd.ExecuteNonQuery();

                        Database.Close();

                        if (success > 0)
                        {
                            module.enabled = false;
                            Say("Disabled the " + paramName + " module.");
                            return true;
                        }
                    }
                    break;
                case "listmodules":
                    {
                        if (!isGlobalOrOwner)
                            return false;

                        var str = "Modules: ";

                        foreach (var moduleBase in IRC.modules)
                        {
                            if(string.IsNullOrWhiteSpace(moduleBase.moduleName.Trim())) continue;
                            str += (moduleBase.enabled ? "☑" : "☐") + moduleBase.moduleName + ", ";
                        }
                        str = str.Substring(0, str.Length - 2);

                        if (str.Length < "Modules: ".Length)
                            str = "No non-base modules. That has to be a bug, contact OrthoLabs.";

                        Say(str);

                        return true;
                    }
            }

            return false;
        }
    }
}
