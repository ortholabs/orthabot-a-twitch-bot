﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrthabotRemake
{
    class Module_CustomCommands : ModuleBase
    {
        public Module_CustomCommands()
        {
            handlesCommands = "Set, Unset, ListCmds";
            moduleName = "CustomCommands";
        }

        public override bool HandleCommand(string name, bool isMod, bool isGlobalOrOwner, string command, string parameters)
        {
            //check if set, unset, or listcmds
            switch (command)
            {
                case "set":
                    {
                        if (!isGlobalOrOwner && !isMod)
                            break;

                        var splitParams = parameters.Trim().Split(' ');
                        if(splitParams.Length > 1) //need at least a name and one thing to respond with
                        {
                            var cmd = splitParams[0];
                            var say = parameters.Substring(cmd.Length + 1);

                            Database.Open();

                            var dbcmd = Database.dbcon.CreateCommand(); dbcmd.CommandText = "REPLACE INTO customCommands (name,say) VALUES (@commandName,@say)";
                            dbcmd.Parameters.AddWithValue("@commandName", cmd);
                            dbcmd.Parameters.AddWithValue("@say", say);
                            var success = dbcmd.ExecuteNonQuery();
                            
                            Database.Close();

                            if(success > 0)
                            {
                                Say("Added " + cmd + " to list of custom commands.");
                                return true;
                            }
                        }
                        break;
                    }
                case "unset":
                    {
                        if (!isGlobalOrOwner && !isMod)
                            break;

                        var cmd = parameters.Trim().Split()[0];

                        Database.Open();

                        var dbcmd = Database.dbcon.CreateCommand(); dbcmd.CommandText = "DELETE FROM customCommands WHERE name = @commandName";
                        dbcmd.Parameters.AddWithValue("@commandName", cmd);
                        var success = dbcmd.ExecuteNonQuery();

                        Database.Close();

                        if (success > 0)
                        {
                            Say("Deleted " + cmd + " from list of custom commands.");
                            return true;
                        }

                        break;
                    }
                case "listcmds":
                    {
                        if (!isGlobalOrOwner && !isMod)
                            break;

                        var str = "Possible custom commands: ";
                        using (var reader = Database.Command("SELECT * FROM customCommands"))
                        {
                            while(reader.Read())
                            {
                                str += (string)reader.data["name"] + ", ";
                            }
                        }
                        str = str.Substring(0, str.Length - 2);

                        if (str.Length < "Possible custom commands: ".Length)
                            str = "No custom commands set! Use the syntax \"!set commandName what you want the bot to say when you use !commandName\"";

                        Say(str);

                        return true;
                    }
                default:
                    {
                        Database.Open();

                        var dbcmd = Database.dbcon.CreateCommand(); dbcmd.CommandText = "SELECT * FROM customcommands WHERE name = @commandName";
                        dbcmd.Parameters.AddWithValue("@commandName", command);
                        var reader = dbcmd.ExecuteReader();
                        dbcmd.Dispose();
                        var found = false;

                        while (reader.Read())
                        {
                            Say((string)reader["say"]);
                            found = true;
                        }

                        reader.Dispose();

                        Database.Close();

                        return found;
                    }
            }

            return false;
        }
    }
}
