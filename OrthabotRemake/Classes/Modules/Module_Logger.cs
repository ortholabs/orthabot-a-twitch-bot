﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace OrthabotRemake
{
    class Module_Logger : ModuleBase
    {
        public string logName = "orthaBotChatlog " + DateTime.Now.Subtract(new DateTime(1970,1,1,0,0,0, DateTimeKind.Utc)).TotalSeconds + ".txt";

        public Module_Logger()
        {
            enabled = false;
            handlesCommands = "";
            moduleName = "Logger";
        }

        public override void FlushedLog(string log)
        {
            File.AppendAllText(logName, log);
        }
    }
}
