﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrthabotRemake
{
    class Module_ArcheAge : ModuleBase
    {
        public Module_ArcheAge()
        {
            handlesCommands = "Archeage";
            moduleName = "Archeage";
        }

        public override bool HandleCommand(string name, bool isMod, bool isGlobalOrOwner, string command, string parameters)
        {
            var usedEvent = false;

            if (command.Equals("archeage") && parameters.Length > 0)
            {
                var seperatedParams = parameters.Split(' ');
                if(seperatedParams.Length == 1) //class to sub-classes
                {
                    var say = LookupSubClasses(parameters.Trim());
                    if (!string.IsNullOrWhiteSpace(say))
                    {
                        Say(say);
                        return true;
                    }
                }
                else if (seperatedParams.Length == 3) //sub-classes to class
                {
                    var say = LookupClass(seperatedParams[0], seperatedParams[1], seperatedParams[2]);
                    if (!string.IsNullOrWhiteSpace(say))
                    {
                        Say(say);
                        return true;
                    }
                }
            }

            return false;
        }

        public enum Classes
        {
            Battlerage,
            Sorcery,
            Archery,
            Vitalism,
            Occultism,
            Shadowplay,
            Defense,
            Auramancy,
            Witchcraft,
            Songcraft,
            none
        }

        #region public static Dictionary<string, Classes> subClassNames
        public static Dictionary<string, Classes> subClassNames = new Dictionary<string, Classes>() {
                                                                {"battlerage", Classes.Battlerage},
                                                                {"battle", Classes.Battlerage},
                                                                {"rage", Classes.Battlerage},
                                                                {"sorcery", Classes.Sorcery},
                                                                {"sorc", Classes.Sorcery},
                                                                {"archery", Classes.Archery},
                                                                {"arch", Classes.Archery},
                                                                {"vitalism", Classes.Vitalism},
                                                                {"vit", Classes.Vitalism},
                                                                {"occultism", Classes.Occultism},
                                                                {"occult", Classes.Occultism},
                                                                {"occ", Classes.Occultism},
                                                                {"shadowplay", Classes.Shadowplay},
                                                                {"shadow", Classes.Shadowplay},
                                                                {"shadowcraft", Classes.Shadowplay},
                                                                {"rogue", Classes.Shadowplay},
                                                                {"defense", Classes.Defense},
                                                                {"def", Classes.Defense},
                                                                {"armor", Classes.Defense},
                                                                {"tank", Classes.Defense},
                                                                {"auramancy", Classes.Auramancy},
                                                                {"aura", Classes.Auramancy},
                                                                {"witchcraft", Classes.Witchcraft},
                                                                {"witch", Classes.Witchcraft},
                                                                {"songcraft", Classes.Songcraft},
                                                                {"song", Classes.Songcraft},
                                                                {"songplay", Classes.Songcraft}
                                                            };
        #endregion

        #region public static Dictionary<string, List<Classes>> classes
        public static Dictionary<string, List<Classes>> classes = new Dictionary<string, List<Classes>>()
                                                           {
                                                               //example
                                                               {"hexblade", new List<Classes>(){Classes.Battlerage, Classes.Witchcraft, Classes.Defense}},
                                                                {"hexwarden", new List<Classes>(){Classes.Battlerage, Classes.Witchcraft, Classes.Auramancy}},
                                                                {"hordebreaker", new List<Classes>(){Classes.Battlerage, Classes.Witchcraft, Classes.Occultism}},
                                                                {"dreadhunter", new List<Classes>(){Classes.Battlerage, Classes.Witchcraft, Classes.Archery}},
                                                                {"harbinger", new List<Classes>(){Classes.Battlerage, Classes.Witchcraft, Classes.Sorcery}},
                                                                {"shadowblade", new List<Classes>(){Classes.Battlerage, Classes.Witchcraft, Classes.Shadowplay}},
                                                                {"dirgeweaver", new List<Classes>(){Classes.Battlerage, Classes.Witchcraft, Classes.Songcraft}},
                                                                {"dervish", new List<Classes>(){Classes.Battlerage, Classes.Witchcraft, Classes.Vitalism}},
                                                                {"abolisher", new List<Classes>(){Classes.Battlerage, Classes.Defense, Classes.Auramancy}},
                                                                {"doomlord", new List<Classes>(){Classes.Battlerage, Classes.Defense, Classes.Occultism}},
                                                                {"liberator", new List<Classes>(){Classes.Battlerage, Classes.Defense, Classes.Archery}},
                                                                {"crusader", new List<Classes>(){Classes.Battlerage, Classes.Defense, Classes.Sorcery}},
                                                                {"blighter", new List<Classes>(){Classes.Battlerage, Classes.Defense, Classes.Shadowplay}},
                                                                {"dawncaller", new List<Classes>(){Classes.Battlerage, Classes.Defense, Classes.Songcraft}},
                                                                {"paladin", new List<Classes>(){Classes.Battlerage, Classes.Defense, Classes.Vitalism}},
                                                                {"bloodreaver", new List<Classes>(){Classes.Battlerage, Classes.Auramancy, Classes.Occultism}},
                                                                {"bonestalker", new List<Classes>(){Classes.Battlerage, Classes.Auramancy, Classes.Archery}},
                                                                {"enforcer", new List<Classes>(){Classes.Battlerage, Classes.Auramancy, Classes.Sorcery}},
                                                                {"darkrunner", new List<Classes>(){Classes.Battlerage, Classes.Auramancy, Classes.Shadowplay}},
                                                                {"herald", new List<Classes>(){Classes.Battlerage, Classes.Auramancy, Classes.Songcraft}},
                                                                {"argent", new List<Classes>(){Classes.Battlerage, Classes.Auramancy, Classes.Vitalism}},
                                                                {"dreadbow", new List<Classes>(){Classes.Battlerage, Classes.Occultism, Classes.Archery}},
                                                                {"ravager", new List<Classes>(){Classes.Battlerage, Classes.Occultism, Classes.Sorcery}},
                                                                {"executioner", new List<Classes>(){Classes.Battlerage, Classes.Occultism, Classes.Shadowplay}},
                                                                {"lorebreaker", new List<Classes>(){Classes.Battlerage, Classes.Occultism, Classes.Songcraft}},
                                                                {"blackguard", new List<Classes>(){Classes.Battlerage, Classes.Occultism, Classes.Vitalism}},
                                                                {"fiendhunter", new List<Classes>(){Classes.Battlerage, Classes.Archery, Classes.Sorcery}},
                                                                {"outrider", new List<Classes>(){Classes.Battlerage, Classes.Archery, Classes.Shadowplay}},
                                                                {"bloodskald", new List<Classes>(){Classes.Battlerage, Classes.Archery, Classes.Songcraft}},
                                                                {"warpriest", new List<Classes>(){Classes.Battlerage, Classes.Archery, Classes.Vitalism}},
                                                                {"hellweaver", new List<Classes>(){Classes.Battlerage, Classes.Sorcery, Classes.Shadowplay}},
                                                                {"spellsword", new List<Classes>(){Classes.Battlerage, Classes.Sorcery, Classes.Songcraft}},
                                                                {"fleshshaper", new List<Classes>(){Classes.Battlerage, Classes.Sorcery, Classes.Vitalism}},
                                                                {"bladedancer", new List<Classes>(){Classes.Battlerage, Classes.Shadowplay, Classes.Songcraft}},
                                                                {"inquisitor", new List<Classes>(){Classes.Battlerage, Classes.Shadowplay, Classes.Vitalism}},
                                                                {"bloodthrall", new List<Classes>(){Classes.Battlerage, Classes.Songcraft, Classes.Vitalism}},
                                                                {"dreambreaker", new List<Classes>(){Classes.Witchcraft, Classes.Defense, Classes.Auramancy}},
                                                                {"defiler", new List<Classes>(){Classes.Witchcraft, Classes.Defense, Classes.Occultism}},
                                                                {"archon", new List<Classes>(){Classes.Witchcraft, Classes.Defense, Classes.Archery}},
                                                                {"cabalist", new List<Classes>(){Classes.Witchcraft, Classes.Defense, Classes.Sorcery}},
                                                                {"shadowknight", new List<Classes>(){Classes.Witchcraft, Classes.Defense, Classes.Shadowplay}},
                                                                {"poxbane", new List<Classes>(){Classes.Witchcraft, Classes.Defense, Classes.Songcraft}},
                                                                {"shadowbane", new List<Classes>(){Classes.Witchcraft, Classes.Defense, Classes.Vitalism}},
                                                                {"nightcloak", new List<Classes>(){Classes.Witchcraft, Classes.Auramancy, Classes.Occultism}},
                                                                {"arcanehunter", new List<Classes>(){Classes.Witchcraft, Classes.Auramancy, Classes.Archery}},
                                                                {"arcanist", new List<Classes>(){Classes.Witchcraft, Classes.Auramancy, Classes.Sorcery}},
                                                                {"eidolon", new List<Classes>(){Classes.Witchcraft, Classes.Auramancy, Classes.Shadowplay}},
                                                                {"enchantrix", new List<Classes>(){Classes.Witchcraft, Classes.Auramancy, Classes.Songcraft}},
                                                                {"heirophant", new List<Classes>(){Classes.Witchcraft, Classes.Auramancy, Classes.Vitalism}},
                                                                {"shadestriker", new List<Classes>(){Classes.Witchcraft, Classes.Occultism, Classes.Archery}},
                                                                {"demonologist", new List<Classes>(){Classes.Witchcraft, Classes.Occultism, Classes.Sorcery}},
                                                                {"shroudmaster", new List<Classes>(){Classes.Witchcraft, Classes.Occultism, Classes.Shadowplay}},
                                                                {"tombcaller", new List<Classes>(){Classes.Witchcraft, Classes.Occultism, Classes.Songcraft}},
                                                                {"necromancer", new List<Classes>(){Classes.Witchcraft, Classes.Occultism, Classes.Vitalism}},
                                                                {"stormcaster", new List<Classes>(){Classes.Witchcraft, Classes.Archery, Classes.Sorcery}},
                                                                {"trickster", new List<Classes>(){Classes.Witchcraft, Classes.Archery, Classes.Shadowplay}},
                                                                {"hexranger", new List<Classes>(){Classes.Witchcraft, Classes.Archery, Classes.Songcraft}},
                                                                {"soulbow", new List<Classes>(){Classes.Witchcraft, Classes.Archery, Classes.Vitalism}},
                                                                {"daggerspell", new List<Classes>(){Classes.Witchcraft, Classes.Sorcery, Classes.Shadowplay}},
                                                                {"lamentor", new List<Classes>(){Classes.Witchcraft, Classes.Sorcery, Classes.Songcraft}},
                                                                {"shaman", new List<Classes>(){Classes.Witchcraft, Classes.Sorcery, Classes.Vitalism}},
                                                                {"nightwitch", new List<Classes>(){Classes.Witchcraft, Classes.Shadowplay, Classes.Songcraft}},
                                                                {"assassin", new List<Classes>(){Classes.Witchcraft, Classes.Shadowplay, Classes.Vitalism}},
                                                                {"athame", new List<Classes>(){Classes.Witchcraft, Classes.Songcraft, Classes.Vitalism}},
                                                                {"skullknight", new List<Classes>(){Classes.Defense, Classes.Auramancy, Classes.Occultism}},
                                                                {"bastion", new List<Classes>(){Classes.Defense, Classes.Auramancy, Classes.Archery}},
                                                                {"thaumaturge", new List<Classes>(){Classes.Defense, Classes.Auramancy, Classes.Sorcery}},
                                                                {"nightblade", new List<Classes>(){Classes.Defense, Classes.Auramancy, Classes.Shadowplay}},
                                                                {"tombwarden", new List<Classes>(){Classes.Defense, Classes.Auramancy, Classes.Songcraft}},
                                                                {"templar", new List<Classes>(){Classes.Defense, Classes.Auramancy, Classes.Vitalism}},
                                                                {"dreadstone", new List<Classes>(){Classes.Defense, Classes.Occultism, Classes.Archery}},
                                                                {"battlemage", new List<Classes>(){Classes.Defense, Classes.Occultism, Classes.Sorcery}},
                                                                {"dreadnaught", new List<Classes>(){Classes.Defense, Classes.Occultism, Classes.Shadowplay}},
                                                                {"darkaegis", new List<Classes>(){Classes.Defense, Classes.Occultism, Classes.Songcraft}},
                                                                {"justicar", new List<Classes>(){Classes.Defense, Classes.Occultism, Classes.Vitalism}},
                                                                {"farslayer", new List<Classes>(){Classes.Defense, Classes.Archery, Classes.Sorcery}},
                                                                {"stonearrow", new List<Classes>(){Classes.Defense, Classes.Archery, Classes.Shadowplay}},
                                                                {"honorguard", new List<Classes>(){Classes.Defense, Classes.Archery, Classes.Songcraft}},
                                                                {"druid", new List<Classes>(){Classes.Defense, Classes.Archery, Classes.Vitalism}},
                                                                {"swiftstone", new List<Classes>(){Classes.Defense, Classes.Sorcery, Classes.Shadowplay}},
                                                                {"earthsinger", new List<Classes>(){Classes.Defense, Classes.Sorcery, Classes.Songcraft}},
                                                                {"scion", new List<Classes>(){Classes.Defense, Classes.Sorcery, Classes.Vitalism}},
                                                                {"nightbearer", new List<Classes>(){Classes.Defense, Classes.Shadowplay, Classes.Songcraft}},
                                                                {"deathwarden", new List<Classes>(){Classes.Defense, Classes.Shadowplay, Classes.Vitalism}},
                                                                {"caretaker", new List<Classes>(){Classes.Defense, Classes.Songcraft, Classes.Vitalism}},
                                                                {"astralranger", new List<Classes>(){Classes.Auramancy, Classes.Occultism, Classes.Archery}},
                                                                {"revenant", new List<Classes>(){Classes.Auramancy, Classes.Occultism, Classes.Sorcery}},
                                                                {"planeshifter", new List<Classes>(){Classes.Auramancy, Classes.Occultism, Classes.Shadowplay}},
                                                                {"phantasm", new List<Classes>(){Classes.Auramancy, Classes.Occultism, Classes.Songcraft}},
                                                                {"edgewalker", new List<Classes>(){Classes.Auramancy, Classes.Occultism, Classes.Vitalism}},
                                                                {"stormchaser", new List<Classes>(){Classes.Auramancy, Classes.Archery, Classes.Sorcery}},
                                                                {"primeval", new List<Classes>(){Classes.Auramancy, Classes.Archery, Classes.Shadowplay}},
                                                                {"howler", new List<Classes>(){Classes.Auramancy, Classes.Archery, Classes.Songcraft}},
                                                                {"oracle", new List<Classes>(){Classes.Auramancy, Classes.Archery, Classes.Vitalism}},
                                                                {"enigmatist", new List<Classes>(){Classes.Auramancy, Classes.Sorcery, Classes.Shadowplay}},
                                                                {"spellsong", new List<Classes>(){Classes.Auramancy, Classes.Sorcery, Classes.Songcraft}},
                                                                {"boneweaver", new List<Classes>(){Classes.Auramancy, Classes.Sorcery, Classes.Vitalism}},
                                                                {"exorcist", new List<Classes>(){Classes.Auramancy, Classes.Shadowplay, Classes.Songcraft}},
                                                                {"soothsayer", new List<Classes>(){Classes.Auramancy, Classes.Shadowplay, Classes.Vitalism}},
                                                                {"cleric", new List<Classes>(){Classes.Auramancy, Classes.Songcraft, Classes.Vitalism}},
                                                                {"spellbow", new List<Classes>(){Classes.Occultism, Classes.Archery, Classes.Sorcery}},
                                                                {"shadehunter", new List<Classes>(){Classes.Occultism, Classes.Archery, Classes.Shadowplay}},
                                                                {"gravesinger", new List<Classes>(){Classes.Occultism, Classes.Archery, Classes.Songcraft}},
                                                                {"bloodarrow", new List<Classes>(){Classes.Occultism, Classes.Archery, Classes.Vitalism}},
                                                                {"reaper", new List<Classes>(){Classes.Occultism, Classes.Sorcery, Classes.Shadowplay}},
                                                                {"requiem", new List<Classes>(){Classes.Occultism, Classes.Sorcery, Classes.Songcraft}},
                                                                {"cultist", new List<Classes>(){Classes.Occultism, Classes.Sorcery, Classes.Vitalism}},
                                                                {"nocturne", new List<Classes>(){Classes.Occultism, Classes.Shadowplay, Classes.Songcraft}},
                                                                {"doombringer", new List<Classes>(){Classes.Occultism, Classes.Shadowplay, Classes.Vitalism}},
                                                                {"sorrowsong", new List<Classes>(){Classes.Occultism, Classes.Songcraft, Classes.Vitalism}},
                                                                {"infiltrator", new List<Classes>(){Classes.Archery, Classes.Sorcery, Classes.Shadowplay}},
                                                                {"evoker", new List<Classes>(){Classes.Archery, Classes.Sorcery, Classes.Songcraft}},
                                                                {"naturalist", new List<Classes>(){Classes.Archery, Classes.Sorcery, Classes.Vitalism}},
                                                                {"ebonsong", new List<Classes>(){Classes.Archery, Classes.Shadowplay, Classes.Songcraft}},
                                                                {"ranger", new List<Classes>(){Classes.Archery, Classes.Shadowplay, Classes.Vitalism}},
                                                                {"soulsong", new List<Classes>(){Classes.Archery, Classes.Songcraft, Classes.Vitalism}},
                                                                {"spellsinger", new List<Classes>(){Classes.Sorcery, Classes.Shadowplay, Classes.Songcraft}},
                                                                {"animist", new List<Classes>(){Classes.Sorcery, Classes.Shadowplay, Classes.Vitalism}},
                                                                {"gypsy", new List<Classes>(){Classes.Sorcery, Classes.Songcraft, Classes.Vitalism}},
                                                                {"confessor", new List<Classes>(){Classes.Shadowplay, Classes.Songcraft, Classes.Vitalism}}
                                                           };
        #endregion

        public static string EnumToString(Classes @class)
        {
            foreach (var subClassName in subClassNames)
            {
                if (subClassName.Value == @class)
                    return subClassName.Key;
            }

            return null;
        }

        public static Classes StringToEnum(string @class)
        {
            foreach (var subClassName in subClassNames)
            {
                if (@class.ToLower().Trim().Equals(subClassName.Key.ToLower()))
                    return subClassName.Value;
            }

            return Classes.none;
        }

        public static string LookupClass(string a, string b, string c) //sub classes -> class
        {
            var aType = StringToEnum(a);
            var bType = StringToEnum(b);
            var cType = StringToEnum(c);

            if (aType == Classes.none ||
               bType == Classes.none ||
               cType == Classes.none ||
               aType == bType ||
               aType == cType ||
               bType == cType)
                return "One of the inputs '" + a + ", " + b + ", or " + c + "' is not in the database, or are the same as eachother.";

            a = a.Insert(1, a[0].ToString().ToUpper());
            a = a.Remove(0, 1);

            b = b.Insert(1, b[0].ToString().ToUpper());
            b = b.Remove(0, 1);

            c = c.Insert(1, c[0].ToString().ToUpper());
            c = c.Remove(0, 1);

            foreach (var @class in classes)
            {
                if (@class.Value.Contains(aType) &&
                   @class.Value.Contains(bType) &&
                   @class.Value.Contains(cType))
                {
                    var cleanInput = @class.Key;

                    a = EnumToString(StringToEnum(a));
                    b = EnumToString(StringToEnum(b));
                    c = EnumToString(StringToEnum(c));

                    a = a.Insert(1, a[0].ToString().ToUpper());
                    a = a.Remove(0, 1);

                    b = b.Insert(1, b[0].ToString().ToUpper());
                    b = b.Remove(0, 1);

                    c = c.Insert(1, c[0].ToString().ToUpper());
                    c = c.Remove(0, 1);

                    cleanInput = cleanInput.Insert(1, cleanInput[0].ToString().ToUpper());
                    cleanInput = cleanInput.Remove(0, 1);

                    return "The sub-types '" + a + ", " + b + ", and " + c + "' make a '" + cleanInput + "'.";
                }
            }

            return "No class with sub classes '" + a + ", " + b + ", and " + c + "' found in database.";
        }

        public static string LookupSubClasses(string input) //class -> sub classes
        {
            foreach (var @class in classes)
            {
                if (@class.Key.ToLower().Equals(input.ToLower().Trim()))
                {
                    var a = EnumToString(@class.Value[0]);
                    var b = EnumToString(@class.Value[1]);
                    var c = EnumToString(@class.Value[2]);

                    var cleanInput = input.ToLower();

                    a = a.Insert(1, a[0].ToString().ToUpper());
                    a = a.Remove(0, 1);

                    b = b.Insert(1, b[0].ToString().ToUpper());
                    b = b.Remove(0, 1);

                    c = c.Insert(1, c[0].ToString().ToUpper());
                    c = c.Remove(0, 1);

                    cleanInput = cleanInput.Insert(1, cleanInput[0].ToString().ToUpper());
                    cleanInput = cleanInput.Remove(0, 1);

                    return cleanInput + "'s sub-classes: " + a + ", " + b + ", " + c;
                }
            }

            return "No class '" + input + "' found in database.";
        }
    }
}
