﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace OrthabotRemake
{
    public class IRC
    {
        public class Config
        {
            public string server = "irc.twitch.tv";
            public int port = 6667;
            public string channel;
            public string name;
        }

        public static List<string> globalMods = new List<string>() { "ortholabs" }; 
        public static List<string> activeMods = new List<string>();

        TcpClient IRCConnection = null;
        public Config config;
        NetworkStream ns = null;
        StreamReader sr = null;
        StreamWriter sw = null;

        public static List<ModuleBase> modules = new List<ModuleBase>();

        static IRC()
        {
            modules.Add(new Module_BaseCommands());
            modules.Add(new Module_ArcheAge());
            modules.Add(new Module_CustomCommands());
            modules.Add(new Module_Logger());
        }

        private Thread work;

        public IRC(Config config)
        {
            this.config = config;
            try
            {
                IRCConnection = new TcpClient(config.server, config.port);
            }
            catch
            {
                BotForm.Write("Connection Error");
            }

            ns = IRCConnection.GetStream();
            sr = new StreamReader(ns);
            sw = new StreamWriter(ns);

            var oauth = "";
            using (var reader = Database.Command("SELECT * FROM config"))
            {
                while(reader.Read())
                {
                    oauth = (string)reader.data["botOauth"];
                }
            }

            SendData("TWITCHCLIENT 2", null);
            SendData("PASS oauth:" + oauth, null);
            SendData("USER OrthaBot 0 * :OrthaBot", null);
            SendData("NICK OrthaBot", null);

            SendData("JOIN #" + config.channel, null);
            SendData("JTVROOMS #" + config.channel, null);
            globalMods.Add(config.channel.ToLower());

            using (var reader = Database.Command("SELECT * FROM modules"))
            {
                while(reader.Read())
                {
                    foreach (var moduleBase in modules)
                    {
                        if(moduleBase.moduleName.Equals((string)reader.data["moduleName"]))
                        {
                            moduleBase.enabled = (int)(Int64)reader.data["moduleEnabled"] == 1;
                        }
                    }
                }
            }

            work = new Thread(IRCWork);
            work.Start();
        }

        public void Cleanup()
        {
            shouldRun = false;

            if (sr != null)
                sr.Close();

            work.Abort();

            if (sw != null)
                sw.Close();
            if (ns != null)
                ns.Close();
            if (IRCConnection != null)
                IRCConnection.Close();
        }

        public void SendData(string cmd, string param)
        {
            var send = cmd + (param == null || param.Trim().Equals("") ? "" : " " + param);
            sw.WriteLine(send);
            sw.Flush();
            if(!send.Contains("PASS oauth"))
                BotForm.Write("Sending: " + send);
            else
                BotForm.Write("Sending: Secure Password"); //hehe, secure password
        }

        string starters = "#@~§&*•○♦✿►◄☞";
        int starterIndex = 0;
        public void Say(string message, string channel = "")
        {
            if (channel.Trim().Equals("")) channel = "#" + config.channel;
            SendData("PRIVMSG ", channel + " :" + starters[starterIndex] + " " + message);
            
            starterIndex++;
            if (starterIndex > starters.Length - 1) starterIndex = 0;
        }

        void HandleDisplayingData(string s)
        {
            if(s.StartsWith(":tmi.twitch.tv"))
            {
                //boring messages
            }
            else if(s.StartsWith(":jtv "))
            {
                if(s.Contains(":USERCOLOR")){}
                else if (s.Contains(":HISTORYEND")){}
                else if (s.Contains(":EMOTESET")){}
                else if (s.Contains(":CLEARCHAT"))
                {
                    BotForm.Write("  -User was purged or banned: " + s.Substring(s.LastIndexOf(" ")), Color.DarkGreen);
                }
                else if (s.Contains("MODE #" + config.channel.ToLower()))
                {
                    var mode = s.Split(' ')[3];

                    if(!mode.Equals("+o") && !mode.Equals("-o"))
                    {
                        BotForm.Write("Need to handle: " + s, Color.Red);
                        return;
                    }

                    var isMod = s.Split(' ')[3].Equals("+o");
                    var name = s.Substring(s.LastIndexOf(" ")).ToLower().Trim();

                    if(!isMod && activeMods.Contains(name)) activeMods.Remove(name);
                    else if (isMod) activeMods.Add(name); 

                    BotForm.Write("  -Mod " + (isMod ? "added" : "removed") + ": " + s.Substring(s.LastIndexOf(" ")), Color.DarkGreen);
                }
                else if(s.Contains(":SPECIALUSER")){/*turbo, admin, mod, staff, whatever*/}
                else
                {
                    BotForm.Write("Need to handle: " + s, Color.Red);
                }
            }
            else if (s.StartsWith(":jtv")) {/*other cases, seems to be before any chat actually happens potentially*/}
            else if (s.StartsWith(":" + config.name)){/*things the bot said, or message for joining chat*/}
            else if (s.Split(' ')[1].Equals("PRIVMSG") && s.Split(' ')[2].Equals("#" + config.channel.ToLower()))
            {
                var name = s.Substring(1, s.IndexOf('!') - 1);
                var msg = s.Substring(s.IndexOf(':', 1) + 1);
                BotForm.Write(name + ": " + msg);
            }
            else if (s.StartsWith("PING")) { /*pong! I am here twitch! :D*/}
            else
            {
                BotForm.Write("Need to handle: " + s, Color.Red);
            }
        }

        private bool shouldRun = true;
        public void IRCWork()
        {
            string[] ex;
            string data = null;
            while (shouldRun)
            {
                try
                {
                    data = sr.ReadLine();
                }catch{}

                if (data == null) continue;

                var charSeperator = new char[]{' '};
                ex = data.Split(charSeperator, 5);

                HandleDisplayingData(data);

                if(ex[0] == "PING")
                {
                    SendData("PONG", ex[1]);
                }

                if (ex.Length > 3 && ex[3].Substring(1).StartsWith("!")) //No parameters or greater
                {
                    var commandUsed = false;
                    var nameOfUser = ex[0].Substring(1).Split('!')[0];
                    var command = ex[3].ToLower().Substring(2);
                    if (string.IsNullOrWhiteSpace(command.Trim())) continue;
                    var param = ""; if (ex.Length > 4) param = ex[4];
                    var isGlobalMod = globalMods.Any(globalMod => globalMod.Equals(nameOfUser));
                    var isMod = false;
                    using (var reader = Database.Command("SELECT * FROM storedMods"))
                    {
                        while(reader.Read())
                        {
                            if (!nameOfUser.ToLower().Equals(reader.data["name"].ToString().ToLower())) continue;
                            isMod = true;
                            break;
                        }
                    }
                    foreach (var activeMod in activeMods)
                    {
                        if (nameOfUser.ToLower().Equals(activeMod))
                        {
                            isMod = true;
                            break;
                        }
                    }

                    foreach (var moduleBase in modules)
                    {
                        if (!moduleBase.enabled) continue;
                        
                        commandUsed = moduleBase.HandleCommand(nameOfUser, isMod, isGlobalMod, command, param.Trim());
                        if (commandUsed) break;
                    }

                    //debug to write to the screen any incoming commands
                    //BotForm.Write(nameOfUser + " - " + command + " - " + param + " - " + isGlobalMod + " - " + isMod);
                }
            }
        }
    }
}
