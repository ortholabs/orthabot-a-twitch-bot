﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OrthabotRemake
{
    public partial class InputBox : Form
    {
        public class InputBoxVariables
        {
            public string title = "WIP: Needs a title.";
            public string button = "Enter";
            public bool passwordMode = false;
            public int width = 265;
        }

        public bool returning = false;
        public string value;

        //required, "Send"
        public InputBox(string name) : this(new InputBoxVariables() {title = name}){}
        public InputBox(string name, bool passwordMode) : this(new InputBoxVariables() { title = name, passwordMode = true}) { }
        public InputBox(InputBoxVariables vars)
        {
            InitializeComponent();
            if (vars.passwordMode) textBox1.PasswordChar = '*';
            Text = vars.title;
            button1.Text = vars.button;
            Size = new Size(vars.width, 98);

            StartPosition = FormStartPosition.Manual;
            Location = new Point((int)(BotForm.instance.Location.X + BotForm.instance.Size.Width/2f - Size.Width/2f),
                                 (int)(BotForm.instance.Location.Y + BotForm.instance.Size.Height/2f - Size.Height/2f));

            ShowDialog();
            Focus();
        }

        void Send()
        {
            var text = textBox1.Text.Trim();
            if (text.Length > 0)
            {
                value = text;
                returning = true;

                Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Send();
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Send();
            }
        }

        private void InputBox_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (returning) return;

            var ret = MessageBox.Show("Exiting Bot", "You can't use this bot without updating your Database. \nClose the bot?",
                            MessageBoxButtons.YesNo);

            if(ret == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                e.Cancel = true;
            }
        }
    }
}
