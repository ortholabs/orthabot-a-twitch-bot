﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace OrthabotRemake
{
    public partial class BotForm : Form
    {
        public static BotForm instance;

        public IRC twitch;

        public BotForm()
        {
            instance = this;
            InitializeComponent();
        }

        private void BotForm_Load(object sender, EventArgs e)
        {

        }

        private void BotForm_Shown(object sender, EventArgs e)
        {
            Database.Init();

            InitIRC();
        }

        public void InitIRC()
        {
            var config = new IRC.Config();
            using (var reader = Database.Command("SELECT * FROM config"))
            {
                while(reader.Read())
                {
                    config.channel = (string)reader.data["channel"];
                    config.name = (string)reader.data["botName"];
                }
            }

            twitch = new IRC(config);
        }

        public void SendCommand(string s)
        {
            Write("Command: " + s);
        }

        private static int maxShownText = 50000;
        private static int clearedText = 10000;
        delegate void WriteCallback(string text, Color col);
        public static void Write(string s, Color color = default(Color))
        {
            var col = Color.Black;
            if (color != default(Color)) col = color;

            if(instance.output.InvokeRequired)
            {
                var d = new WriteCallback(Write);
                instance.Invoke(d, new object[] {s, col});
            }
            else
            {
                foreach (var moduleBase in IRC.modules)
                {
                    if(moduleBase.enabled)
                        moduleBase.AddedLog(s, col);
                }

                instance.output.SelectionColor = col;

                instance.output.AppendText(s + "\n");

                if (instance.output.Text.Length > maxShownText)
                {
                    foreach (var moduleBase in IRC.modules)
                    {
                        if (moduleBase.enabled)
                            moduleBase.FlushedLog(instance.output.Text.Substring(0, clearedText));
                    }

                    instance.output.Text = instance.output.Text.Substring(clearedText);
                    instance.output.AppendText(" ");
                }
            }
        }

        private void input_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                var text = input.Text.Trim();
                if(text.Length > 0)
                {
                    SendCommand(text);
                    input.Text = "";
                }
            }
        }

        private void BotForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach (var moduleBase in IRC.modules)
            {
                moduleBase.FlushedLog(instance.output.Text);
            }

            twitch.Cleanup();
        }
    }
}
